"use strict";
// none of this is designed to support ie.
// in order to support ie may consider writing a separate code or extensively going over with fine tooth comb
// wherever we have new windows coming in we need to consider loading the script into them;
(function () {
	var w = window;
	function markWindow(focusWindow) {
		focusWindow.PQXTlocalIdentifier = currentGlobalIdentifier();
	};
	function windowMarked(focusWindow) {
		if(focusWindow.PQXTlocalIdentifier && focusWindow.PQXTlocalIdentifier == currentGlobalIdentifier()) return true;
		return false;
	};
	function currentGlobalIdentifier () {
		return window.PQXTglobalIdentifier;
	};
	function nextGlobalIdentifier () {
		window.PQXTglobalIdentifier += 1;
	};
	function recurseWindows() {
		console.log("Recursing");
		window.everyElement = [];
		nextGlobalIdentifier();
		var windowStack = [];
		windowStack.push(window);
		while(windowStack.length > 0) {
			var thisWindow = windowStack.pop();
			if(windowMarked(thisWindow)) continue;
			markWindow(thisWindow);
			thisWindow.pqxtMainWindow = window.pqxtMainWindow || window;
			watchMutations(thisWindow);
			setUpMutationNotifications(thisWindow); // consider loading scrip into here
			var thisWindowDocument = thisWindow.document || thisWindow.documentElement;
			if(thisWindowDocument) extractWindowsAndElements(thisWindow,windowStack,everyElement);
		};	
	};
	function watchMutations(inThisWindow) {
		var windowDocument = inThisWindow.document || inThisWindow.documentElement;
		if(windowDocument) pqxtWatch(inThisWindow,mutationCallback(),windowDocument);
	}
	function extractWindows(sourceWindow,windowStash) {
		for(var i = 0; i < sourceWindow.frames.length; i += 1 ) {
			var subFrame = sourceWindow.frames[i];
			if(!subFrame) continue;
			var childWindow = subFrame.window;
			if(!childWindow) continue;
			windowStash.push(childWindow);			
		}
	};
	function extractElements(sourceWindow,elementStash) {
		var theDocument = sourceWindow.document || sourceWindow.documentElement;
		if(!theDocument) return;
		elementStash.push(theDocument);
		var theDocumentsElements = theDocument.getElementsByTagName('*');
		for(var i = 0; i < theDocumentsElements.length; i += 1 ) {
			var childElement = theDocumentsElements[i];
			if(!childElement) continue;
			elementStash.push(childElement);
		}			
	}
	function extractWindowsAndElements(sourceWindow,windowStash,elementStash) {
		extractWindows(sourceWindow,windowStash);
		extractElements(sourceWindow,elementStash);
	};
	w.runpqxt = function () {
		recurseWindows();
		console.log("No. of elements " + everyElement.length);
	};
	w.runpqxtonload = function () {
		if(w.pqxtalreadyrun) return;
		w.pqxtalreadyrun = 1;
		w.runpqxt();
	};
	w.addEventListener('load',w.runpqxtonload);
	w.runpqxtonload();

	// mutation handling
	// mutation gives us a root we need to recurse from that root and find everything new
	function extractWindowFromPotentialFrame(potentialFrame,windowStash) {
		if(potentialFrame.constructor == HTMLFrameElement || potentialFrame.constructor == HTMLIFrameElement) {
			var windowExists = potentialFrame.window || potentialFrame.contentWindow;
			if(windowExists) windowStash.push(windowExists);	
		}
	};
	function extractWindowsAndElementsFromRoot(theRoot,newWindows,newElements) {
		var theRootsElements = theRoot.getElementsByTagName('*');
		extractWindowFromPotentialFrame(theRoot,newWindows);	
		for(var i = 0; i < theRootElements.length; i += 1) {
			var newElement = theRootElements[i];
			newElements.push(newElement);
			extractWindowFromPotentialFrame(newElement,newWindows);
		}
	}
	function handleNewElementsFromRoot(theRoot) {
		nextGlobalIdentifier();
		var newWindows = [];
		var newElements = [];
		extractWindowsAndElementsFromRoot(theRoot,newWindows,newElements);
		while(newWindows.length > 0) {
			var newWindow = newWindows.pop();
			if(!newWindow || windowMarked(newWindow)) continue;
			markWindow(newWindow);
			newWindow.pqxtMainWindow = window.pqxtMainWindow || window;
			setUpMutationNotifications(newWindow);//consider loading us into here
			extractWindowsAndElements(newWindow,newWindows,newElements);			
		}
		return newElements;
	}
	function alternativeMutationWatch(aWindow,theDoc) {
		// check for duplicates
		aWindow.newNodes = [];
		aWindow.removedNodes = [];
		theDoc.addEventListener('DOMNodeInserted', handleNodeInsertedCreate(aWindow), false );
		theDoc.addEventListener('DOMNodeRemoved', handleNodeRemovedCreate(aWindow), false );
	}
	function handleNodeInsertedCreate(aWindow) {
		return function(e) {
			var elem = e.target;
			aWindow.newNodes.push(elem);
			if(aWindow.pqxtNodeInsertedTimeout) aWindow.clearTimeout(aWindow.pqxtNodeInsertedTimout);
			aWindow.pqxtNodeInsertedTimeout = aWindow.setTimeout(processInserted(aWindow),1200); 
		};
	}
	function processInserted(aWindow) {
		return function() {
			if(aWindow.newNodes.length > 0)
				handleNewElements(aWindow.newNodes);
		};
	}
	function handleNodeRemovedCreate(aWindow) {
		return function(e) {
			var elem = e.target;
			aWindow.removedNodes.push(elem);
			if(aWindow.pqxtNodeRemovedTimeout) aWindow.clearTimeout(aWindow.pqxtNodeRemovedTimeout);
			aWindow.pqxtNodeRemovedTimeout = aWindow.setTimeout(processRemoved(aWindow),1200);
		};
	}
	function processRemoved(aWindow) {
		return function() {
			if(aWindow.removedNodes.length > 0) 
				handleRemovedElements(aWindow.removedNodes);
		};
	}
	function pqxtWatch(wd,callback,docElem) {
		var mutationObserverSource = wd.WebKitMutationObserver || wd.MozMutationObserver || wd.MutationObserver;
		if(!mutationObserverSource) {
			console.log("Setting up alternative mutation watch");
			alternativeMutationWatch(wd,docElem);	
			return undefined;
		}
		console.log("Setting up mutation watch");
		var observer = new mutationObserverSource(callback);
		observer.observe(docElem, { childList : true } );  
		return observer;
	}
	function handleNewElementsFromRoots(targets) {
		console.log("Targets affected " + targets.length);
		while(targets.length > 0) {
			var target = targets.pop();
		}	
	}
	function handleRemovedElements(removedElements) {
		console.log("Removed elements " + removedElements.length );
		while(removedElements.length > 0) {
			var removedElem = removedElements.pop();
		}
	}
	function handleNewElements(newElements) {
		console.log("New elements " + newElements.length);
		while(newElements.length > 0) {
			var newElem = newElements.pop();
		}
	}
	function mutationCallback() {
		return function (mutations) {
			console.log("Processing mutations");
			var targets = [];
			var newElements = [];
			var oldElements = [];		
			mutations.forEach(
				function(mutation) {
					targets.push(mutation.target);
					var mutationadded = mutation.addedNodes;
					for(var i = 0; i < mutationadded.length; i += 1 ) {
						newElements.push(mutationadded[i]);
					}
					var mutationremoved = mutation.removedNodes;
					for(var i = 0; i < mutationremoved.length; i += 1 ) {
						removedElements.push(mutationremoved[i]);
					}
				}
			);	
			console.log("Handling changes from mutations");
			handleNewElementsFromRoots(targets);
			handleNewElements(newElements);
			handleRemovedElements(removedElements);
		};
	}
	// insert mutation just deal with the new inserts
	// if element has donotprocess flag, switch back to process flag. 
	// if element has been processed by us before, ignore
	// otherwise add the right methods, properties, etc
	// delete mutation change the elements flag to do not process
	w.pqxtNotifyMutated = function() {
		console.log("Re-running because of unload -- consider loading script");
		//w.runpqxt();
		w.pqxtnotified = 1;
		if(w.pqxtMainWindow && !w.pqxtMainWindow.pqxtnotified && w.pqxtMainWindow.pqxtNotifyMutated) w.pqxtMainWindow.pqxtNotifyMutated();
		else w.pqxtMainWindow.runpqxt();
	};
	function setUpMutationNotifications(aWindow) {
		aWindow.addEventListener('unload',(function(aaWindow,aaWindowMain) { 
			return function(e) {
				if(aaWindow != aaWindowMain) aaWindowMain.pqxtNotifyMutated(); 
			};
		})(aWindow,aWindow.pqxtMainWindow),true);
		aWindow.pqxtnotified = undefined;
	}
})();	 
