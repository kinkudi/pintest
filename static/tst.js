// should compute size, dim of element using padding as well
// should ignore elements that contain nothing or are transparent.
// should use mutation event, workers, and SVG
var xqjct = function(){
	(function() {
	var w=window;
	if(!w.liconfo)
		w.licinfo = {
		'Public Domain' : { 'color' : '', 'desc': 'Pin, post, remix, sell in anyway.', 'site': 'http://en.wikipedia.org/wiki/Public_domain', 'logo':'https://pintestme.appspot.com/images/copyfree.png', 'ensign' : 'This work is in the public domain.' },
		'Copyfree' : { 'color' : '', 'desc' : 'Pin, post, remix, sell in anyway.', 'site' : 'http://en.wikipedia.org/wiki/Free_content#Copyfree', 'logo' : 'https://pintestme.appspot.com/images/copyfree.png', 'ensign' : 'This work is <a class=ptbqlink href=http://en.wikipedia.org/wiki/Free_content#Copyfree>copyfree</a>. If you possess this work, you own this work, and you are free to copy, share, modify and dispose of it as you wish without any outside interference.' },
		'Beer-Ware' : { 'color' : '', 'desc': 'Pin,post,remix,sell in anyway.', 'site' : 'http://people.freebsd.org/~phk/', 'logo' : 'https://pintestme.appspot.com/images/copyfree.png', 'ensign' : '"THE BEER-WARE LICENSE" (Revision 42):<br> <b contenteditable=true tabindex=1>[email/s]</b> made this file. As long as you retain this notice you can do whatever you want with this stuff. If we meet some day, and you think this stuff is worth it, you can buy me a beer in return <b contenteditable=true tabindex=2>[name/s]</b>' },
		'WTFPL' : { 'color' : '', 'desc': 'Pin, post, remix, sell in anyway.', 'site' : 'http://en.wikipedia.org/wiki/WTFPL', 'logo' : 'https://pintestme.appspot.com/images/copyfree.png', 'ensign' : '<p align=center>WHAT THE FUCK YOU WANT TO PUBLIC LICENSE<br>Version 2, December 2004<br>Copyright (C) 2004 Sam Hocevar <sam@hocevar.net></p><p>Everyone is permitted to copy and distribute verbatim or modified copies of this license document, and changing it is allowed as long as the name is changed.</p><p align=center>\t\tDO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE<br>TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION</p><p style="text-align: center">0. You just DO WHAT THE FUCK YOU WANT TO.</p>'
},
		'CrimethInc. NⒸ! license'  : { 'color' : '', 'desc': 'Pin, post, remix, sell in order to better the world as long as you are not a government or a corporation.', 'site': 'http://www.crimethinc.com', 'logo' : 'https://pintestme.appspot.com/svg/CrimeThink.svg', 'ensign' : 'NⒸ! <b contenteditable=true tabindex=1>[year/s]</b> The publishers, the notorious <b contenteditable=true tabindex=2>[name/s]</b> humbly put this work and all its contents in the disposal of those who, in good faith, might use, circulate, plagiarize, revise and otherwise make use of them in the course of making the world a better place.<br>Possession, reproduction, transmission, excerpting, introduction as evidence in court, and all other applications by any government, corporation, security organization, or similar party of evil intent, are strictly prohibited and punishable under natural law.'},
		'Free Art License' : { 'color' : '', 'desc': 'Pin, post, remix, no sell but credit me and license all downstream works under this license.', 'site' : 'http://artlibre.org/license/lal/en', 'logo' : 'https://pintestme.appspot.com/images/ArteLibre.png', 'ensign' : 'Copyleft <b contenteditable=true tabindex=1>[name/s] [year/s]</b>.<br>This is a free work, you can copy, distribute and modify it under the terms of the Free Art License <a class=ptbqlink href=http://artlibre.org/license/lal/en/>http://artlibre.org/license/lal/en/</a>' }, 
		'Against DRM 2.0' : { 'color' : '', 'desc': 'Pin, post, remix, sell in anyway but never stop anyone to repin,repost,remix or resell my work, and license all pins,posts,remixes or sales of my work and its derivatives with this license.', 'site': 'http://www.freecreations.org/Against_DRM2.html', 'logo' : 'https://pintestme.appspot.com/images/Against_DRM_2.png', 'ensign' : '<b contenteditable=true tabindex=1>[Original work]</b> Copyright (C) <b contenteditable=true tabindex=2>[year/s] [name/s of author/s]</b>.<br><b contenteditable=true tabindex=3>[Work]</b> Copyright (C) <b contenteditable=true tabindex=4>[year/s] [name/s of author/s]</b>.<br><b contenteditable=true tabindex=5>[Derivative work]</b> Copyright (C) <b contenteditable=true tabindex=6>[year/s] [name/s of author/s]</b>.<p>Licensee must keep intact copyright notice and all notices that refer to this license. Licensee must include a copy of this license with every copy of the work the licensee distributes, publicly demonstrates or publicly performs. This work is licensed under Against DRM 2.0</p>' },
		'CC0' : { 'color' : '', 'desc' : 'Pin, post, remix, sell in anyway', 'site' :'http://creativecommons.org/publicdomain/zero/1.0/', 'logo' :'https://pintestme.appspot.com/images/cc0.png', 'ensign' : 'To the extent possible under the law <b contenteditable=true tabindex=7>[name/s]</b> has waived all copyright and related or neighbouring rights to <b contenteditable=true tabindex=8>[work/s]</b>.' },
		'CC BY' : { 'color' : '', 'desc': 'Pin, post, remix, sell in anyway but credit me', 'site': 'http://creativecommons.org/licenses/by/3.0/', 'logo': 'https://pintestme.appspot.com/images/ccby.png', 'ensign' : '<b contenteditable=true tabindex=1>[work/s]</b> by <b contenteditable=true tabindex=2>[name/s or website/s]</b> is licensed under a <a class=ptbqlink href=http://creativecommons.org/licenses/by/3.0/>Creative Commons Attribution 3.0 Unported License</a>.'},
	        'CC BY-SA' : { 'color' : '', 'desc' : 'Pin, post, remix, sell in anyway but license with CC BY-SA and credit me', 'site' : 'http://creativecommons.org/licenses/by-sa/3.0/', 'logo': 'https://pintestme.appspot.com/images/bysa.png', 'ensign' : '<b contenteditable=true tabindex=1>[work/s]</b> by <b contenteditable=true tabindex=2>[name/s or website/s]</b> is licensed under a <a class=ptbqlink href=http://creativecommons.org/licenses/by-sa/3.0/>Creative Commons Attribution-ShareAlike 3.0 Unported License.</a>'},
		'CC BY-ND' : { 'color' : '', 'desc' : 'Pin, post, no remix, sell but credit me', 'site': 'http://creativecommons.org/licenses/by-nd/3.0/', 'logo': 'https://pintestme.appspot.com/images/bynd.png', 'ensign' : '<b contenteditable=true tabindex=1>[work/s]</b> by <b contenteditable=true tabindex=2>[name/s or website/s]</b> is licensed under a <a class=ptbqlink href=http://creativecommons.org/licenses/by-nd/3.0/>Creative Commons Attribution-NoDerivs 3.0 Unported License</a>.'},
		'CC BY-NC' : { 'color' : '', 'desc': 'Pin, post, remix, no sell but credit me', 'site': 'http://creativecommons.org/licenses/by-nc/3.0/', 'logo': 'https://pintestme.appspot.com/images/bync.png', 'ensign' : '<b contenteditable=true tabindex=1>[work/s]</b> by <b contenteditable=true tabindex=2>[name/s or website/s]</b> is licensed under a <a class=ptbqlink href=http://creativecommons.org/licenses/by-nc/3.0/>Creative Commons Attribution-NonCommercial 3.0 Unported License</a>.'},
		'CC BY-NC-SA' : { 'color' : '', 'desc' : 'Pin, post, remix, no sell but license with CC-BY-NC-SA and credit me', 'site': 'http://creativecommons.org/licenses/by-nc-sa/3.0/', 'logo': 'https://pintestme.appspot.com/images/byncsa.png', 'ensign' : '<b contenteditable=true tabindex=1>[work/s]</b> by <b contenteditable=true tabindex=2>[name/s or website/s]</b> is licensed under a <a class=ptbqlink href=http://creativecommons.org/licenses/by-nc-sa/3.0/>Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License</a>.'},
		'CC BY-NC-ND' : { 'color' : '', 'desc': 'Pin, post, no remix, no sell but credit me', 'site': 'http://creativecommons.org/licenses/by-nc-nd/3.0/', 'logo': 'https://pintestme.appspot.com/images/byncnd.png', 'ensign' : '<b contenteditable=true tabindex=1>[work/s]</b> by <b contenteditable=true tabindex=2>[name/s or website/s]</b> is licensed under a <a class=ptbqlink href=http://creativecommons.org/licenses/by-nc-nd/3.0/>Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License</a>.'},
		'Copyright' : { 'color' : '', 'desc':'Pin, post, remix, sell, only when you have my approval unless it is Fair Use', 'site': 'http://en.wikipedia.org/wiki/Copyright', 'logo': 'https://pintestme.appspot.com/images/copyright.png', 'ensign' : '<a class=ptbqlink href=http://en.wikipedia.org/wiki/Copyright/>Copyright</a> <b contenteditable=true tabindex=1>[name/s] [year/s]</b>.' },
		'Fair Use (US and Israel only)' : { 'color' : '', 'desc': 'Pin, post, remix, sell without my consent if you judge your use to :<p style="padding-left: 2em">1. add new value to my work, not simply derive value from it.<br>2. use more from my work which is factual, and less from work which is fictional and use more from my published work and less from my unpublished work.<br>3. use less of the significant parts of my work, and use more of the insignificant parts, unless you are creating a parody.<br>4. not decrease income I may get from my work when the market of my work and your work could overlap.</p>', 'site' : 'http://fairuse.stanford.edu/', 'logo': 'https://pintestme.appspot.com/images/copyright.png', 'ensign' : 'Copyright <b contenteditable=true tabindex=1>[name/s] [year/s]</b>. Used under <a class=ptbqlink href=http://fairuse.stanford.edu/>Fair Use</a>.' },
		'Single Use' : { 'color' : '', 'desc' : 'Pin, post, remix or sell my work for one project only in only the ways I permit, and only after agreeing to pay my fee.', 'site' : '', 'logo' : 'https://pintestme.appspot.com/images/copyright.png', 'ensign' : 'Copyright <b contenteditable=true tabindex=1>[name/s][year/s]</b>. This work is licensed under a single-use license. To use this work you must obtain a license from <b contenteditable=true tabindex=2>[email/s][website]</b>.' },
		'Royalty-free' : { 'color' : '', 'desc' : 'Pin, post, remix, sell my work in only the ways I permit, and only after agreeing to pay my one-time fee.', 'site' : '' , 'logo' : 'https://pintestme.appspot.com/images/copyright.png', 'ensign' : 'Copyright <b contenteditable=true tabindex=1>[name/s][year/s]</b>. This work is licensed under a royalty free license. To use this work you must obtain a license from <b contenteditable=true tabindex=2>[email/s][website]</b>.' }, 	
		'Rights Managed' : { 'color' : '', 'desc' : 'Pin, post, remix, sell my work in only the ways I permit, and only after agreeing to pay my fee.', 'site' : '', 'logo' : 'https://pintestme.appspot.com/images/copyright.png', 'ensign' : 'Copyright <b contenteditable=true tabindex=1>[name/s][year/s]</b>. This work has rights reserved. You may only use this work after obtaining a license from <b contenteditable=true tabindex=2>[email/s][website]</b>.' },
	};
	var d=document;var ce = d.createElement;var tn=d.createTextNode;
	if(!d.body)
		d.body = d.getElementsByTagName('body')[0];
	var styles = d.createElement('style');
	styles.innerHTML = ".ptbqh3 { display: block !important; vertical-align: middle; position: relative; padding: 0 !important; margin: 0 !important; background-image: none !important; background-color: transparent !important; color: black !important; font: 16pt Helvetica !important; } #pintestliccontainer {border: '10px';} .ptbqlink {color: blue;}";
	d.head.insertBefore(styles,d.head.firstChild);
	function show_detail(title,detail) {
		uphtml(dp.title,title);
		uphtml(dp.desc, detail.desc);
		uphtml(dp.ensign,detail.ensign);
		dp.ensign.style.backgroundColor = detail.color;
		dp.ensign.style.color = 'black';
		dp.ensign.style.font = '10pt Helvetica';
		if(detail.logo.match('svg$')) {
			dp.logo.style.display = 'none';
			dp.logos.data = detail.logo;
			dp.logos.style.display = 'inline';
		}
		else {
			dp.logos.style.display = 'none';
			dp.logo.style.display = 'inline';
			dp.logo.src = detail.logo;
		}
		//alert(detail.color);
		dp.container.style.backgroundColor = detail.color;
		dp.site.href = detail.site;	
		dp.chosen = 0;
		var maxtabs = dp.container.getElementsByTagName('b');
		maxtabs = maxtabs[maxtabs.length-1];
		if(maxtabs)
			dp.savebtn.tabIndex = maxtabs.tabIndex+1;
		else
			dp.savebtn.tabIndex = 1;
		dp.savebtn.style.display = 'block';
		uphtml(dp.site,"About");
	};
	function opt(select,text) {
		var opt = d.createElement("div");
		opt.style.font = "12pt Helvetica";
		opt.appendChild(d.createTextNode(text));
		opt.style.padding = "2px";
		opt.style.color = "black";
		opt.style.border = "1px solid gray";
		opt.style.backgroundColor = w.licinfo[opt.textContent].color;
		opt.onmouseover = function(e) {
			var lic = opt.textContent;
			if(w.claimon==1)
				w.disableparentscroll = 1;
			opt.style.color = "white";
			show_detail(lic,w.licinfo[lic]);
		};
		opt.onmouseout = function(e) {
			var lic = opt.textContent;
			if(w.claimon==1)
				w.disableparentscroll = 0;
			opt.style.color = "black";
		};
		select.appendChild(opt);	
	};
	var mySelect = d.createElement("div");
	var maxColor = 280; 
	var run = 0;
	var delta = 16; 
	for ( var lic in w.licinfo ) {
		w.licinfo[lic].color ='hsla('+(maxColor - run*delta)+',100%,50%,0.8)';
		opt(mySelect,lic);
		run += 1;
	}
	var dp = {};

	function build_detail_panel() {
		dp.container = d.createElement('div');
		dp.title = d.createElement('h3');
		dp.desc = d.createElement('p');
		dp.site = d.createElement('a');
		dp.logo = d.createElement('img');
		dp.logo.align = "left";
		dp.logo.style.padding = "3px";
		dp.logo.style.maxWidth = "20%";
		dp.logos = d.createElement('object');
		dp.logos.align = "left";
		dp.logos.style.padding = "3px";
		dp.logos.style.maxWidth = "20%";
		dp.logos.width = 120;
		dp.logos.height = 80;
		dp.ensign = d.createElement('blockquote');
		dp.ensign.style.textAlign = 'center';
		dp.container.appendChild(dp.logo);
		dp.container.appendChild(dp.logos);
		dp.container.appendChild(dp.title);
		dp.container.appendChild(dp.site);
		dp.container.appendChild(dp.desc);
		dp.container.appendChild(dp.ensign);
		dp.container.style.textAlign = "left";
		dp.container.style.width = "50%";
		dp.container.style.position = "fixed";
		dp.container.style.zIndex = 2000000;
		//dp.container.style.overflow = "auto";
		dp.container.style.padding = "10px";
		dp.container.style.border = "3px solid black";
		dp.container.style.left = "27%";
		dp.container.style.top = "5%";
		dp.container.style.font = "10pt Helvetica";
		dp.container.style.color = "black";
		dp.site.style.color = "gray";
		dp.title.className = 'ptbqh3';
		dp.container.style.backgroundColor = "rgba(50,50,50,0.9)";
		dp.savebtn = d.createElement('input');
		dp.savebtn.type = 'submit';
		dp.savebtn.value = 'Claim';
		dp.savebtn.style.display = 'none';
		
		dp.savebtn.onclick = function(e) {
			var c_uri, c_body;
			if(savexml_inprogress==1) 
				alert("Recording of your claim is in progress");
			else {
				alert("Your claim is being recorded");
				var savexml = new XMLHttpRequest();
				savexml.onreadystatechange = function () {
					if(savexml.readyState == 4 && savexml.status == 200) {
						alert("Computer says: " + savexml.responseText);	

					}
					else {
						//alert(savexml.readyState);
					}
				};
				savexml.open("POST","/app/claim",true);
				savexml.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
				var savexml_inprogress = 1;
				savexml.send('content_uri='+encodeURIComponent(c_uri)+'&content_body='+encodeURIComponent(c_body));		
			}
		};
		dp.container.appendChild(dp.savebtn);
		d.body.appendChild(dp.container);
	};
	build_detail_panel();
	function uptext(elem, text) {
		while(elem.childNodes.length > 0) {
			elem.removeChild(elem.firstChild);
		}
		elem.appendChild(d.createTextNode(text));	
	};
	function uphtml(elem, text) {
		elem.innerHTML = text;
	};
	function optionchanged(e) {
		var detail = w.licinfo[e.target.value];
		if(detail) {
			dp.container.style.display = "none";
			show_detail(e.target.value, detail);
			dp.container.style.display = "inline";
		}

	};
	w.claimon = 0;
	d.body.appendChild(mySelect);
	mySelect.style.maxWidth = "20%";
	mySelect.style.overflowX = "hidden";
	mySelect.style.height = "90%";
	mySelect.style.overflowY = "auto";
	mySelect.style.top = "5%";
	mySelect.style.left = "5%";
	mySelect.style.position = "fixed";
	mySelect.style.zIndex = 200000;
	mySelect.style.display = "none";
	dp.container.style.display = "none";
//alert("Starting");
if(w.pintestcn) {
	//alert("W.cn");
	if(w.ptqoff == 1) {
		//alert("W.off==1");
		w.ptqoff = 0;
		thisp = w.pintestcn.getContext('2d');	
		w.pintestcn.style.display = "inline";
		w.pineverything.print();
		w.pinjustpics.print();
		logoDiv();
		//alert("PinTest.Me Reloaded");
		return;
	}
	else {
		//alert("W.off==0");
		java();
	}
	//alert("Going back");
	return;
}
else
	w.pintestcn = d.createElement('canvas');
if(!w.ptqoff) {
	w.pintestcn.id = 'pintestcanvas';
	d.body.appendChild(w.pintestcn);
	w.pintestcn.width = window.innerWidth;
	w.pintestcn.height = window.innerHeight;
	w.pintestcn.style.position = "fixed";
	w.pintestcn.style.left = 0;
	w.pintestcn.style.top = 0;
	w.pintestcn.style.zIndex = 100000;
	//w.pintestcn.style.pointerEvents = "none";
	var thisp = w.pintestcn.getContext('2d');
//g.fillRect(0,0,w.pintestcn.width,w.pintestcn.height);
	w.ptqoff = 0;
}
if(!thisp) 
	var thisp;

function allbtn() {
	this.text = "ALL";
	this.bt = d.createElement('div');
	d.body.appendChild(this.bt);
	this.bt.style.marginLeft = "2px";
	this.bt.style.display = "none";
	this.bt.style.backgroundColor = "deeppink";
	this.bt.style.top = 0;
	this.bt.style.lineHeight = "100%";
	this.bt.style.left = "65%";
	this.bt.style.height = "5%";
	this.bt.style.width = "10%";
	this.bt.style.color = "white";
	this.bt.style.fontSize = Math.round(0.04*window.innerHeight) + "px";
	this.bt.style.fontFamily = "Helvetica";
	this.bt.style.verticalAlign = "middle !important";
	this.bt.style.paddingTop = "3px";
	this.bt.style.textAlign = "center";
	this.bt.textContent = this.text;
	this.bt.style.position = "fixed";
	this.bt.style.zIndex = 100000000;
	this.curleft = Math.round(0.65*window.innerWidth);
	this.curtop = 0;
	this.maxleft = Math.round(0.75*window.innerWidth);
	this.maxtop = Math.round(0.05*window.innerHeight);
	this.bt.style.display = "inline";
	return this;
};
allbtn.prototype.inside = function(e) {
	var x = e.clientX || e.pageX;
	var y = e.clientY || e.pageY;
	//alert(x+","+y +","+this.curleft+","+this.curtop+","+this.maxleft+","+this.maxtop);
	return (x >= this.curleft && y >= this.curtop && x <= this.maxleft && y <= this.maxtop);
};
allbtn.prototype.print = function () {
	this.bt.style.display = "inline";
	return this;
};
allbtn.prototype.click = function(e) {
	w.pintestimagesonly = 0;
	return this;
};
function justpicsbtn() {
	this.text = "PIXONLY";
	this.bt = d.createElement('div');
	d.body.appendChild(this.bt);
	this.bt.style.display = "none";
	this.bt.style.marginLeft = "2px";
	this.bt.style.backgroundColor = "deeppink";
	this.bt.style.top = 0;
	this.bt.style.lineHeight = "100%";
	this.bt.style.left = "55%";
	this.bt.style.height = "5%";
	this.bt.style.width = "10%";
	this.bt.style.color = "white";
	this.bt.style.fontSize = Math.round(0.04*window.innerHeight) + "px";
	this.bt.style.fontFamily = "Helvetica";
	this.bt.style.verticalAlign = "middle !important";
	this.bt.style.paddingTop = "3px";
	this.bt.style.textAlign = "center";
	this.bt.textContent = this.text;
	this.bt.style.position = "fixed";
	this.bt.style.zIndex = 100000000;
	this.curleft = Math.round(0.55*window.innerWidth);
	this.curtop = 0;
	this.maxleft = Math.round(0.65*window.innerWidth);
	this.maxtop = Math.round(0.05*window.innerHeight);
	this.bt.style.display = "inline";
	return this;
};
justpicsbtn.prototype.inside = function(e) {
	var x = e.clientX || e.pageX;
	var y = e.clientY || e.pageY;
	//alert(x+","+y +","+this.curleft+","+this.curtop+","+this.maxleft+","+this.maxtop);
	return (x >= this.curleft && y >= this.curtop && x <= this.maxleft && y <= this.maxtop);
};
justpicsbtn.prototype.print = function () {
	this.bt.style.display = "inline";
	return this;
};
justpicsbtn.prototype.click = function(e) {
	w.pintestimagesonly = 1;
	return this;
};
function logoDiv() {
	if(w.ptqlogo) {
		w.ptqlogo.style.display = "inline";
		return;
	}
	w.ptqlogo = d.createElement('div');
	d.body.appendChild(w.ptqlogo);
	w.ptqlogo.style.display = "none";
	w.ptqlogo.style.lineHeight = "100%";
	w.ptqlogo.style.backgroundColor = "deeppink";
	w.ptqlogo.style.top = 0;
	w.ptqlogo.style.left = "45%";
	w.ptqlogo.style.height = "5%";
	w.ptqlogo.style.width = "10%";
	w.ptqlogo.style.color = "white";
	w.ptqlogo.style.fontSize = Math.round(0.04*window.innerHeight) + "px";
	w.ptqlogo.style.fontFamily = "Helvetica";
	w.ptqlogo.style.verticalAlign = "middle !important";
	w.ptqlogo.style.paddingTop = "3px";
	w.ptqlogo.style.textAlign = "center";
	w.ptqlogo.textContent = "PINTEST";
	w.ptqlogo.style.position = "fixed";
	w.ptqlogo.style.zIndex = 100000000;
	w.ptqlogo.style.display = "inline";
	if(!w.pinjustpics) {
		w.pineverything = new allbtn();
		w.pinjustpics = new justpicsbtn();
	}
	else {
		w.pineverything.print();
		w.pinjustpics.print();
	}
};
function logo() {
	thisp.fillStyle = "rgba(50,50,50,0.9)";
	thisp.fillRect(w.pintestcn.width/2-40,0,80,20);
	thisp.strokeStyle = "rgba(50,50,50,0.4)";
	thisp.lineWidth = 3;
	thisp.strokeRect(w.pintestcn.width/2-40,0,80,20);
	thisp.textAlign = "center";
	thisp.fillStyle = "white";
	thisp.font = "10pt Helvetica";
	thisp.fillText("PINTEST",w.pintestcn.width/2,15);
	if(!w.pinjustpics) {
		w.pinjustpics = new justpicsbtn();
	}
	else
		w.pinjustpics.print();
};
if(w.ptqoff == 1) {
	w.ptqoff = 0;
	thisp = w.pintestcn.getContext('2d');	
	w.pintestcn.style.display = "inline";
	logoDiv();//logo();
	//alert("PinTest.Me Reloaded");
	return;
}
function getPos(m) {
	var om = m;
	if(!m || m == null)
		return;
	var curleft = 0, curtop = 0;
	var pqtzi = 0;
	// there's an error here in moz maybe need to work out why
	var mcs;
	try {	
		mcs = w.getComputedStyle(m);
	}
	catch(err) {
		mcs = undefined;
	}
	if(mcs && mcs.getPropertyValue('z-index')) {
		var mcsz = parseInt(mcs.getPropertyValue('z-index'));
		if(mcsz >= pqtzi)
			pqtzi = mcsz;
	}
	else if(m.style && m.style.zIndex) {
		if(m.style.zIndex >= ptqzi)
			pqtzi = m.style.zIndex;		
	}
	if (m.offsetParent) {
		do {
			try {
				mcs = w.getComputedStyle(m);
			}
			catch(err) {
				mcs = undefined;
			}
			if(mcs && mcs.getPropertyValue('z-index')) {
				var mcsz = parseInt(mcs.getPropertyValue('z-index'));
				if(mcsz >= pqtzi)
					pqtzi = mcsz;
			}
			else if(m.style && m.style.zIndex) {
				if(m.style.zIndex >= ptqzi)
					pqtzi = m.style.zIndex;		
			}
			if(m.constructor == HTMLIFrameElement || m.constructor == HTMLFrameElement) {
				var mwindow = m.window || m.contentWindow;
				if(mwindow) {
					if(!om.ptqwindowscrollables)
						om.ptqwindowscrollables = [];
					om.ptqwindowscrollables.push(mwindow);
				}
			}	
			curleft += m.offsetLeft;
			curtop += m.offsetTop;
			if(m != d.body) {
				if(m.scrollLeft != undefined || m.scrollTop != undefined) {
					if(!om.ptqscrollables)
						om.ptqscrollables = [];
					om.ptqscrollables.push(m);
				}
				
			}
		} while (m = m.offsetParent);
	}
	om.curleft = curleft;
	om.curtop = curtop;
	om.pqtzi = pqtzi;
	return om;
};
w.dlen = { 'images' : d.images.length, 'all': 0 };
function java() {
	if(!w.currentptqprocessed)
		w.currentptqprocessed=0;
	w.currentptqprocessed += 1;
	if(w.ptqlogo) {
		w.ptqlogo.ptqprocessed = w.currentptqprocessed;
		w.pineverything.bt.ptqprocessed = w.currentptqprocessed;
		w.pinjustpics.bt.ptqprocessed = w.currentptqprocessed;
	}
	var dd = d.getElementsByTagName('*');
	var ddq = [];
	ddq.push(dd);
	ddq.push(w.frames);
	// i should rewrite with a proper traverse, like in insert script
	w.dlen.all = 0;
	while(ddq.length > 0) {
		dd = ddq.pop();
		for (var i = 0; i < dd.length; i+=1) {
			var j = dd[i];
			if(j.id == 'pintestcanvas')
				continue;
			if(j.constructor == HTMLIFrameElement || j.constructor == HTMLFrameElement) {
				var jd;
				try {
					jd = j.document || j.documentElement || j.getContentDocument || j.contentWindow ?j.contentWindow.document : undefined; 
				}
				catch(xdo_error) { jd = undefined; }
				if(!jd)
					1;
				else {
					var dda = jd.getElementsByTagName('*');
					dda = Array.prototype.slice.call(dda);	
					dda.push(jd);
					ddq.push(dda);
				}
			}
			w.dlen.all += 1;
			getPos(j);
			j.dimj = j.offsetWidth*j.offsetHeight;
			if(j.style && j.style != null)
				j.style.pointerEvents = ""; // do we need this?
			j.maxleft = j.curleft + j.offsetWidth;
			j.maxtop = j.curtop + j.offsetHeight;
			j.inside = (function(that) {
				return function(e) {
					var x = e.clientX || e.pageX;
					var y = e.clientY || e.pageY;
					var bs = w.getComputedStyle(d.body);
					if(bs.overflowX != 'hidden')	
						x += window.scrollX;
					if(bs.overflowY != 'hidden')		
						y += window.scrollY;
					if(that.ptqscrollables) {
						for(var si in that.ptqscrollables) {
							var pqitem = that.ptqscrollables[si]; 
							x += pqitem.scrollLeft || 0;
							y += pqitem.scrollTop || 0;
						}
					}
					/**
					 * Handle code for parents that are scrollables windows (so frames and iframes)
					 * */
					if(that.ptqwindowscrollables) {
						for(var wi in that.ptqwindowscrollables){
							var pqtw = that.ptqwindowscrollables[wi];
							x += pqtw.scrollX || 0;
							y += pqtw.scrollY || 0;
						}
					}
					return (x >= that.curleft && y >= that.curtop && x <= that.maxleft && y <= that.maxtop);
				};
			})(j);
		}
	}
};
java();

function checkbtn() {
	this.w = 160;
	this.h = 50;
	this.curleft = w.pintestcn.width-196;
	this.curtop = w.pintestcn.height/5;
	this.maxleft = this.curleft + this.w;
	this.maxtop = this.curtop + this.h;
	return this.print();
};
checkbtn.prototype.print = function() {
	thisp.fillStyle = "hsla(130,100%,50%,0.9)"
	thisp.fillRect(this.curleft,this.curtop,this.w,this.h);
	thisp.textAlign = "center";
	thisp.fillStyle = "white";
	thisp.font = "20pt Helvetica";
	thisp.fillText("Check",this.curleft+80,this.curtop+30);
	return this;
};
checkbtn.prototype.inside = function(e) {
	var x = e.clientX || e.pageX;
	var y = e.clientY || e.pageY;
	return (x >= this.curleft && y >= this.curtop && x <= this.maxleft && y <= this.maxtop);
};
function claimbtn() {
	this.w = 160;
	this.h = 50;
	this.curleft = w.pintestcn.width-196;
	this.curtop = w.pintestcn.height/5+68;
	this.maxleft = this.curleft + this.w;
	this.maxtop = this.curtop + this.h;
	return this.print();
};
claimbtn.prototype.print = function() {
	thisp.fillStyle = "hsla(0,100%,50%,0.9)"
	thisp.fillRect(this.curleft,this.curtop,this.w,this.h);
	thisp.textAlign = "center";
	thisp.fillStyle = "white";
	thisp.font = "20pt Helvetica";
	thisp.fillText("Claim",this.curleft+80,this.curtop+30);
	return this;
};
claimbtn.prototype.inside = function(e) {
	var x = e.clientX || e.pageX;
	var y = e.clientY || e.pageY;
	return (x >= this.curleft && y >= this.curtop && x <= this.maxleft && y <= this.maxtop);
};
claimbtn.prototype.printclaimdialogue = function(e) {
	prompt("This is a claim dialgoue");
};
checkbtn.prototype.click = function() {
	if(w.pintesttext) {
		alert("Check on " + w.pintesttext);
		var c_uri, c_body;
		if(savexml_inprogress==1) 
			alert("Checking of a claim is in progress");
		else {
			alert("The claim is being checked");
			var savexml = new XMLHttpRequest();
			savexml.onreadystatechange = function () {
				if(savexml.readyState == 4 && savexml.status == 200) {
					alert("Computer says: " + savexml.responseText);	

				}
				else {
					//alert(savexml.readyState);
				}
			};
			savexml.open("POST","/app/check",true);
			savexml.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
			var savexml_inprogress = 1;
			savexml.send('content_uri='+encodeURIComponent(c_uri)+'&content_body='+encodeURIComponent(c_body));		
		}
	}
};
claimbtn.prototype.click = function() {
	if(w.pintesttext) {
		w.claimon = 1;
		w.pintestclaimsavedX = w.pageXOffset;
		w.pintestclaimsavedY = w.pageYOffset;
		mySelect.style.display = "inline";
		dp.container.style.display = "inline";
	}
};
function checkall(e) {
	if(!w.ptqlistid)
		w.ptqlistid = 0;
	w.ptqlistid += 1;
	if(w.ptqoff==1 || w.claimon == 1)
		return true;
	if(w.pinjustpics && w.pinjustpics.inside(e)) {
		w.pinjustpics.click();
		return;
	}
	if(w.pineverything && w.pineverything.inside(e)) {
		w.pineverything.click();
		return;
	}
	if(w.pcleared==0) {
		if(w.pinclaim && w.pinclaim.inside(e)) {
			w.pinclaim.click();
			return false;
		}
		else if(w.pincheck && w.pincheck.inside(e)) {
			w.pincheck.click();
			return false;
		}
	}
	release(e);
	e.stopPropagation();
	e.preventDefault();
	if(w.ptqlogo) {
		w.ptqlogo.ptqlistid = w.ptqlistid;
		w.pineverything.bt.ptqlistid = w.ptqlistid;
		w.pinjustpics.bt.ptqlistid = w.ptqlistid;
	}
	var dd;
	var ddq = []
	dd = d.getElementsByTagName('*');
	ddq.push(dd);
	ddq.push(w.frames);
	var ddq2 = [];
	while(ddq.length > 0) {
		dd = ddq.pop();
		for(var ei = 0; ei < dd.length; ei += 1) {
			var elem = dd[ei];
			if(elem.ptqlistid == w.ptqlistid)
				continue;
			else
				elem.ptqlistid = w.ptqlistid;
			if(elem.id == 'pintestcanvas')
				continue;
			if(elem.constructor == HTMLIFrameElement || elem.constructor == HTMLFrameElement ) {
				var jd;
				try {
					jd = elem.document || elem.documentElement || elem.getContentDocument || elem.contentWindow ?elem.contentWindow.document : undefined; 
				}
				catch(xdo_err) { jd = undefined;}
				if(jd) {
					var dda = jd.getElementsByTagName('*');
					dda = Array.prototype.slice.call(dda);	
					dda.push(jd);
					ddq.push(dda);	
				}
			}
			ddq2.push(elem);
		}			
	}
	if(!w.pintestimagesonly || w.pintestimagesonly==0) {
		if(w.dlen.all != ddq2.length) {
			java();
		}
	}
	else {
		ddq2 = d.images;
		if(w.dlen.images != ddq2.length) {
			java();
		}
	}
	if(ddq2.length == 0)
		return;
	var smallestz = 0;
	var smallest = ddq2[0].dimj;
	smallest = smallest < 100 ? 100 : smallest;
	var smallestj = ddq2[0];
	var selected = 0;
	for(var i in ddq2) {
		var j = ddq2[i];
		if(j.inside && j.inside(e) ) {
			selected = 1;
			if(w.pintestimagesonly==1) {
				smallestj = j;
				break;
			}
			var jz = j.pqtzi || 0;
			if( jz >= smallestz ) {
				smallestz = jz;
				smallestj = j;
			}
			if( j.dimj <= smallest && j.dimj >= 100) {
				smallest = j.dimj;
				smallestj = j;
			}
		}
	}
	if(selected == 1) {
		w.ptsmallestj = smallestj;
		attest(smallestj);
	}	
	return false;
};
function attest(j) {
	w.pcleared = 0;
	var maskColor = "rgba(50,50,50,0.9)";
	var maskInverse = "rgba(255,255,255,1.0)";
	var strokeColor = "rgba(100,100,100,0.7)";
	var joinStyle = "bevel";
	var gco = thisp.globalCompositeOperation;
	var x = j.curleft;
	var y = j.curtop;
	var bs = w.getComputedStyle(d.body);
	if(bs.overflowX != 'hidden')	
		x -= w.scrollX;
	if(bs.overflowY != 'hidden')	
		y -= w.scrollY;
	if(j.ptqscrollables) {
		for(var si in j.ptqscrollables) {
			var pqitem = j.ptqscrollables[si];
			x -= pqitem.scrollLeft || 0;
			y -= pqitem.scrollTop || 0;
		}
	}
	thisp.fillStyle = maskColor;
	thisp.fillRect(0,0,thisp.canvas.width,thisp.canvas.height);
	thisp.globalCompositeOperation = "xor";
	thisp.fillStyle = maskInverse;
	thisp.fillRect(x,y,j.offsetWidth,j.offsetHeight);
	thisp.globalCompositeOperation = gco;
	//thisp.lineJoin = this.joinStyle;
	//thisp.strokeStyle = this.strokeColor;
	//thisp.lineWidth = 4;
	//thisp.strokeRect(x,y,j.offsetWidth,j.offsetHeight);
	if(j.src != null && j.src.length > 0)
		w.pintesttext = j.src;
	else
		w.pintesttext = j.textContent.replace(/[^\w.(),?!~*'"-]+/g,' ');
	if(!w.pincheck)
		w.pincheck = new checkbtn();
	else
		w.pincheck.print();
	if(!w.pinclaim)
		w.pinclaim = new claimbtn();
	else
		w.pinclaim.print();
}
function release(e) {
	if(w.disableparentscroll == 1) {
		w.scrollTo(w.pintestclaimsavedX,w.pintestclaimsavedY);
		return false;
	}
	if(w.ptqoff && w.ptqoff == 1)
		return true;
	w.ptsmallestj = undefined;
	if(d.body.style.overflow != 'scroll') {
		d.body.style.overflow = 'scroll';
	}
	if(!w.pcleared || w.pcleared == 0) {
		w.claimon = 0;
		mySelect.style.display = 'none';
		dp.container.style.display = 'none';
		thisp.clearRect(0,0,w.pintestcn.width,w.pintestcn.height);
		logoDiv();
	}
	w.pcleared = 1;
};

w.addEventListener("mousedown",checkall);
w.addEventListener("scroll",release);
w.addEventListener("resize",function(e) {
	if(w.pintestresizing && w.pintestresizing != null) {
		clearTimeout(w.pintestresizing);
	}
	w.pintestresizing = setTimeout(function () {
				w.pintestcn.width = w.innerWidth;
				w.pintestcn.height = w.innerHeight;
				w.pincheck = undefined;
				w.pinclaim = undefined;
				if(w.ptqlogo) {
					d.body.removeChild(w.pineverything.bt);
					d.body.removeChild(w.pinjustpics.bt);
					d.body.removeChild(w.ptqlogo);
				}
				w.pinevertyhing = undefined;
				w.pinjustpics = undefined;
				w.ptqlogo = undefined;
				java();
				if(w.ptsmallestj) {
					attest(w.ptsmallestj);
				}
				logoDiv();//logo();
				w.pintestresizing = null;
	}, 500);
});
function offt(e) {
	//alert(e.target.constructor);
	//alert(e.target.contentEditable);
	//alert(e.target == dp.container);
	//perhaps a getPos and inside for dp.container?
	// or a class naem if its inherited
	if(e.target.constructor == HTMLInputElement || e.target.constructor == HTMLAnchorElement || e.target.contentEditable == 'true' || e.target == dp.container || e.target.offsetParent == dp.container) {
		e.stopPropagation();
		e.preventDefault();
		return true;
	}
	w.ptqoff ^= 1;
	if(w.ptqoff == 1) {
		mySelect.style.display = "none";
		dp.container.style.display = "none"; 
		w.claimon = 0;
		w.disableparentscroll = 0;
		w.pintestcn.style.display = "none";
		if(w.ptqlogo) {
			w.ptqlogo.style.display = "none";
			w.pineverything.bt.style.display = "none";
			w.pinjustpics.bt.style.display = "none";
		}
	}
	else {
		w.pintestcn.style.display = "inline";
		if(w.ptqlogo) {
			w.ptqlogo.style.display = "inline";
			w.pineverything.print();
			w.pinjustpics.print();
		}
		logoDiv();//logo();
	}
	return true;
};
w.addEventListener("dblclick",offt);
release(0);
//alert("PinTest.Me loaded");
})();
};

